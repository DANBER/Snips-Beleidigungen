#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import random

from assistant import Assistant

# ======================================================================================================================

assist = Assistant()
insults = None
safe_names = None
file_path = os.path.dirname(os.path.realpath(__file__)) + "/"


# ======================================================================================================================

def load_insults(lang):
    path = file_path + "insults/" + lang.lower() + ".txt"
    with open(path) as file:
        # Read line by line and remove whitespace characters at the end of each line
        content = file.readlines()
        content = [x.strip() for x in content]

    random.shuffle(content)
    return content


# ======================================================================================================================

def get_insult():
    """ Get first entry and append it to the end -> no repetition until every entry selected """

    insult = insults.pop(0)
    insults.append(insult)
    return insult


# ======================================================================================================================

def callback_insult(hermes, intent_message):
    if (intent_message.slots is not None and len(intent_message.slots.name) >= 1):
        name = str(intent_message.slots.name.first().value)
        line = get_insult()

        if (name.lower() in safe_names):
            result_sentence = assist.get_text("protect_person").format(name, line)
        else:
            result_sentence = assist.get_text("insult").format(name, line)
    else:
        result_sentence = assist.get_text("not_understood")

    hermes.publish_end_session(intent_message.session_id, result_sentence)


# ======================================================================================================================

insults = load_insults(assist.get_config()["secret"]["language"])
safe_names = [sn.lower() for sn in assist.get_config()["secret"]["safe_names"]]

if __name__ == "__main__":
    h = assist.get_hermes()
    h.connect()
    h.subscribe_intent("DANBER:beschimpfe", callback_insult)
    h.loop_forever()
